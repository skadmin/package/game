<?php

declare(strict_types=1);

namespace Skadmin\Game\Doctrine\Game;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\DateTime;
use Skadmin\TournamentGame\Doctrine\Tournament\Tournament;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Game
{
    use Entity\Id;
    use Entity\WebalizeName;
    use Entity\Content;
    use Entity\IsActive;
    use Entity\ImagePreview;
    use Entity\Sequence;
    use Entity\Website;

    #[ORM\Column]
    private string $shortName = '';

    #[ORM\Column(nullable: true)]
    private ?string $imageGamePreview = '';

    /** @var ArrayCollection|Collection|Tournament[] */
    #[ORM\OneToMany(targetEntity: Tournament::class, mappedBy: 'game')]
    private $tournaments;

    public function __construct()
    {
        $this->tournaments = new ArrayCollection();
    }

    public function getShortName(bool $force = false): string
    {
        if ($force) {
            return $this->shortName;
        }

        return $this->shortName !== '' ? $this->shortName : $this->name;
    }

    public function getImageGamePreview(): ?string
    {
        if ($this->imageGamePreview !== null) {
            return $this->imageGamePreview === '' ? null : $this->imageGamePreview;
        }

        return $this->imageGamePreview;
    }

    public function update(string $name, string $shortName, bool $isActive, string $content, string $website, ?string $imagePreview, ?string $imageGamePreview): void
    {
        $this->name      = $name;
        $this->shortName = $shortName;
        $this->isActive  = $isActive;
        $this->content   = $content;
        $this->website   = $website;

        if ($imagePreview !== null && $imagePreview !== '') {
            $this->imagePreview = $imagePreview;
        }

        if ($imageGamePreview === null || $imageGamePreview === '') {
            return;
        }

        $this->imageGamePreview = $imageGamePreview;
    }

    /**
     * @return ArrayCollection|Collection|Tournament[]
     */
    public function getTournaments(bool $onlyActive = false)
    {
        if (! $onlyActive) {
            return $this->tournaments;
        }

        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('isActive', true));

        $criteria->orderBy(['termFrom' => 'DESC']);

        return $this->tournaments->matching($criteria);
    }

    /**
     * @return ArrayCollection|Collection|Tournament[]
     */
    public function getTournamentsInFuture(bool $onlyActive = false)
    {
        if (! $onlyActive) {
            return $this->tournaments;
        }

        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('isActive', true));
        $criteria->andWhere(Criteria::expr()->gte('termTo', new DateTime()));

        $criteria->orderBy(['termFrom' => 'ASC']);

        return $this->tournaments->matching($criteria);
    }

    /**
     * @return ArrayCollection|Collection|Tournament[]
     */
    public function getTournamentsInPast(bool $onlyActive = false, int $limit = 6, int $offset = 0)
    {
        if (! $onlyActive) {
            return $this->tournaments;
        }

        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('isActive', true));
        $criteria->andWhere(Criteria::expr()->lt('termTo', new DateTime()));

        $criteria->setFirstResult($offset);
        $criteria->setMaxResults($limit);
        $criteria->orderBy(['termFrom' => 'DESC']);

        return $this->tournaments->matching($criteria);
    }
}
