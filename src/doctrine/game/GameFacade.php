<?php

declare(strict_types=1);

namespace Skadmin\Game\Doctrine\Game;

use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits\Facade;

final class GameFacade extends Facade
{
    use Facade\Webalize;
    use Facade\Sequence;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = Game::class;
    }

    public function create(string $name, string $shortName, bool $isActive, string $content, string $website, ?string $imagePreview, ?string $imageGamePreview): Game
    {
        return $this->update(null, $name, $shortName, $isActive, $content, $website, $imagePreview, $imageGamePreview);
    }

    public function update(?int $id, string $name, string $shortName, bool $isActive, string $content, string $website, ?string $imagePreview, ?string $imageGamePreview): Game
    {
        $game = $this->get($id);
        $game->update($name, $shortName, $isActive, $content, $website, $imagePreview, $imageGamePreview);

        if (! $game->isLoaded()) {
            $game->setWebalize($this->getValidWebalize($name));
            $game->setSequence($this->getValidSequence());
        }

        $this->em->persist($game);
        $this->em->flush();

        return $game;
    }

    public function get(?int $id = null): Game
    {
        if ($id === null) {
            return new Game();
        }

        $game = parent::get($id);

        if ($game === null) {
            return new Game();
        }

        return $game;
    }

    /**
     * @param array<string> $orderBy
     *
     * @return array<Game>
     */
    public function getAll(bool $onlyActive = false, array $orderBy = ['sequence' => 'ASC']): array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }

    public function findByWebalize(string $webalize): ?Game
    {
        $criteria = ['webalize' => $webalize];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }
}
