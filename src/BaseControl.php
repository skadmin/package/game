<?php

declare(strict_types=1);

namespace Skadmin\Game;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE  = 'game';
    public const DIR_IMAGE = 'game';

    public function getMenu(): ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-gamepad']),
            'items'   => ['overview'],
        ]);
    }
}
