<?php

declare(strict_types=1);

namespace Skadmin\Game\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\Game\BaseControl;
use Skadmin\Game\Doctrine\Game\Game;
use Skadmin\Game\Doctrine\Game\GameFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class Edit extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory $webLoader;
    private GameFacade    $facade;
    private Game          $game;
    private ImageStorage  $imageStorage;

    public function __construct(?int $id, GameFacade $facade, Translator $translator, LoaderFactory $webLoader, LoggedUser $user, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);
        $this->facade       = $facade;
        $this->webLoader    = $webLoader;
        $this->imageStorage = $imageStorage;

        $this->game = $this->facade->get($id);
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->game->isLoaded()) {
            return new SimpleTranslation('game.edit.title - %s', $this->game->getName());
        }

        return 'game.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->image_preview, BaseControl::DIR_IMAGE);

        // IDENTIFIER
        $gameIdentifier = UtilsFormControl::getImagePreview($values->image_game_preview, BaseControl::DIR_IMAGE);

        if ($this->game->isLoaded()) {
            if ($identifier !== null && $this->game->getImagePreview() !== null) {
                $this->imageStorage->delete($this->game->getImagePreview());
            }

            if ($gameIdentifier !== null && $this->game->getImageGamePreview() !== null) {
                $this->imageStorage->delete($this->game->getImageGamePreview());
            }

            $game = $this->facade->update(
                $this->game->getId(),
                $values->name,
                $values->short_name,
                $values->is_active,
                $values->content,
                $values->website,
                $identifier,
                $gameIdentifier
            );
            $this->onFlashmessage('form.game.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $game = $this->facade->create(
                $values->name,
                $values->short_name,
                $values->is_active,
                $values->content,
                $values->website,
                $identifier,
                $gameIdentifier
            );
            $this->onFlashmessage('form.game.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'send_back') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit',
            'id'      => $game->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ]);
    }

    public function render(): void
    {
        $template               = $this->getComponentTemplate();
        $template->imageStorage = $this->imageStorage;
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/edit.latte');

        $template->game = $this->game;
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.game.edit.name')
            ->setRequired('form.game.edit.name.req');
        $form->addText('short_name', 'form.game.edit.short-name');
        $form->addText('website', 'form.game.edit.website');
        $form->addCheckbox('is_active', 'form.game.edit.is-active')
            ->setDefaultValue(true);
        $form->addImageWithRFM('image_preview', 'form.game.edit.image-preview');
        $form->addImageWithRFM('image_game_preview', 'form.game.edit.image-game-preview');

        // TEXT
        $form->addTextArea('content', 'form.game.edit.content');

        // BUTTON
        $form->addSubmit('send', 'form.game.edit.send');
        $form->addSubmit('send_back', 'form.game.edit.send-back');
        $form->addSubmit('back', 'form.game.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->game->isLoaded()) {
            return [];
        }

        return [
            'name'       => $this->game->getName(),
            'short_name' => $this->game->getShortName(true),
            'content'    => $this->game->getContent(),
            'is_active'  => $this->game->isActive(),
            'website'    => $this->game->getWebsite(),
        ];
    }
}
