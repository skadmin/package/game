<?php

declare(strict_types=1);

namespace Skadmin\Game\Components\Admin;

use App\Model\Grid\Traits\IsActive;
use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Html;
use Skadmin\Game\BaseControl;
use Skadmin\Game\Doctrine\Game\Game;
use Skadmin\Game\Doctrine\Game\GameFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class Overview extends GridControl
{
    use APackageControl;
    use IsActive;

    private GameFacade    $facade;
    private LoaderFactory $webLoader;

    public function __construct(GameFacade $facade, Translator $translator, User $user, LoaderFactory $webLoader)
    {
        parent::__construct($translator, $user);

        $this->facade    = $facade;
        $this->webLoader = $webLoader;
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'game.overview.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [$this->webLoader->createJavaScriptLoader('jQueryUi')];
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel()->orderBy('a.sequence', 'ASC'));

        // COLUMNS
        $grid->addColumnText('name', 'grid.game.overview.name')
            ->setRenderer(function (Game $game): Html {
                if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'edit',
                        'id'      => $game->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href'  => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                $name->setText($game->getName());

                return $name;
            });
        $grid->addColumnText('shortName', 'grid.game.overview.short-name');
        $grid->addColumnText('website', 'grid.game.overview.website')
            ->setRenderer(static function (Game $game): ?Html {
                if ($game->getWebsite() === '') {
                    return null;
                }

                $icon = Html::el('small', ['class' => 'fas fa-external-link-alt mr-1']);

                return Html::el('a', [
                    'href'   => $game->getWebsite(),
                    'target' => '_blank',
                ])->setHtml($icon)
                    ->addText($game->getWebsite());
            });
        $this->addColumnIsActive($grid, 'game.overview');

        // FILTER
        $grid->addFilterText('name', 'grid.game.overview.name', ['name']);
        $this->addFilterIsActive($grid, 'game.overview');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addAction('edit', 'grid.game.overview.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // TOOLBAR
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addToolbarButton('Component:default', 'grid.game.overview.action.new', [
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // OTHER
        $grid->setDefaultSort(['name' => 'ASC']);
        $grid->setDefaultFilter(['isActive' => true]);

        // SORTING
        $grid->setSortable();
        $grid->setSortableHandler($this->link('sort!'));

        return $grid;
    }

    public function handleSort(?string $itemId, ?string $prevId, ?string $nextId): void
    {
        $this->facade->sort($itemId, $prevId, $nextId);

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $presenter->flashMessage('grid.game.overview.action.flash.sort.success', Flash::SUCCESS);
        }

        $this['grid']->reload();
    }
}
