<?php

declare(strict_types=1);

namespace Skadmin\Game\Components\Admin;

interface IOverviewFactory
{
    public function create(): Overview;
}
