<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210317155647 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'game.overview', 'hash' => '5599bf4145b457745e7858011b415518', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Hry', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.game.title', 'hash' => '522bc8a50396f0ae090cbd73685ea4c2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Hry', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.game.description', 'hash' => '442c8bb3729333074029ace5064b71e1', 'module' => 'admin', 'language_id' => 1, 'singular' => 'umožňuje spravovat hry', 'plural1' => '', 'plural2' => ''],
            ['original' => 'game.overview.title', 'hash' => '22b8f24fe005183d13f4d9876cc1ef18', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Hry|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.game.overview.is-active', 'hash' => '8ba83cf5276489973dad5aacaec6532f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.game.overview.action.new', 'hash' => '588daa01a056571f25e1c0c7cbf70600', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit hru', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.game.overview.name', 'hash' => '92700c05eba42fbc254beb475e3cd169', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.game.overview.short-name', 'hash' => '7f2521928afdaf86364481c84b19e78b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zkratka', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.game.overview.website', 'hash' => '631afb6c711c2ffd83529c95a64cbc51', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Odkaz', 'plural1' => '', 'plural2' => ''],
            ['original' => 'game.edit.title', 'hash' => 'dd9bab3dbcbe93ffe9e8b0122a8c9fa4', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení hry', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.game.edit.name', 'hash' => '4b9a9b06481ccde391a1ade97c5a8e7b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.game.edit.name.req', 'hash' => '36cff725030404bbbeedd2782aa1d733', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.game.edit.short-name', 'hash' => 'c95e9cb8a5babc17a80cea499f0520b2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zkratka', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.game.edit.website', 'hash' => '6c9e78534772046fc4797a15dc0f5073', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Odkaz', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.game.edit.image-preview', 'hash' => '1174c5ba5615f286824a32e5888b68b7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Náhled loga hry', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.game.edit.image-preview.rule-image', 'hash' => '877086feb4ef65306cc833850ee45ff7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vybraný soubor není platný obrázek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.game.edit.image-game-preview', 'hash' => '7691cb8fbf05f546cc8939fda74ba351', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Náhled hry', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.game.edit.image-game-preview.rule-image', 'hash' => '3b71b4b0d5c5709f5f83c94dd1307ec5', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vybraný soubor není platný obrázek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.game.edit.content', 'hash' => '88dc79a27b052922a2b0ab657c89c61c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.game.edit.is-active', 'hash' => '733774f77705040fa27fa834f21f60a4', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.game.edit.send', 'hash' => '07f23817e8d3d75850fb8412f059e56f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.game.edit.send-back', 'hash' => 'ffe8d7e6e71f144968bffcfcd2241cb7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.game.edit.back', 'hash' => 'bbb5022fbd15dafdea771e9ae7180812', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'game.edit.title - %s', 'hash' => '263aeb91bfc1fa6b6c6639ed052e38c6', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace hry', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.game.edit.flash.success.create', 'hash' => 'dab88490be5e487c442572e036271c7d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Hry byla úspěšně založena.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.game.edit.flash.success.update', 'hash' => 'e914a2f6f36f197f279d90c0807047d3', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Hry byla úspěšně upravena.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.game.overview.action.edit', 'hash' => '46a68244b3f5532c17019d6a7f040d35', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
